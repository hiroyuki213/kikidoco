class Track < ActiveRecord::Base
    
    
    has_many :favorites, :as => :resource
    
    class << self
    def info
        
    end
    
    def ranking_datas
    
    require 'net/http'
    require 'uri'
    require 'json'
    
    url = URI.parse('https://itunes.apple.com/jp/rss/topsongs/limit=100/json')
    json = Net::HTTP.get(url)
    results = JSON.parse(json)
    
        results["feed"]["entry"].each do |entry|
    
            track_id = entry["id"]["attributes"]["im:id"]
            preview_url = entry["link"][1]["attributes"]["href"]
            
            Track.create(itunes_track_id: track_id, preview_url: preview_url)
            
    
        end

    end
    end
end
