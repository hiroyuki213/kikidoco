class ItunesApiTestController < ApplicationController
    
    def index
        
        require 'itunes-search-api'
        
        results = ITunesSearchAPI.search(:term => "Uver", :country => "JP", :media => "music")
        
        result = results.first
        
        respond_to do |format|
                format.html { render :text => "ok", :status => 200 }
                format.json { render :json => results.to_json }
        end
    
    end
    
end
